package com.example.test.entity.dataObj;

import com.example.test.entity.attrObj.RsrvModRqstAttr;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class RsrvModRqstData {
    @SerializedName("ds_rsrvInfo")
    List<RsrvModRqstAttr> list;
}
