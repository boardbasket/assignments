package com.example.test.entity.dataObj;

import com.example.test.entity.attrObj.RsrvRqstAttr;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@Getter
@Setter
@ToString
public class RsrvRqstData {
    @SerializedName("ds_rsrvInfo")
    List<RsrvRqstAttr> list;
}
