package com.example.test.entity.dataObj;

import com.example.test.entity.attrObj.RsrvCnclRqstAttr;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class RsrvCnclRqstData {

    @SerializedName("ds_cnclInfo")
    private List<RsrvCnclRqstAttr> list;
}
