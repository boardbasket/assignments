package com.example.test.entity.headerObj;

import com.example.test.entity.attrObj.RsrvModRqstAttr;
import com.example.test.entity.dataObj.RsrvModRqstData;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class RsrvModRqstHeader {
    @SerializedName("Data")
    private RsrvModRqstData dataObj;

    public RsrvModRqstHeader(HttpServletRequest request) {
        String custNo = request.getParameter("CUST_NO");
        String custIdntNo = request.getParameter("CUST_IDNT_NO");
        String rsrvNo = request.getParameter("RSRV_NO");
        String arrvDate = request.getParameter("ARRV_DATE");
        Integer ovntCnt = null;
        try {
            ovntCnt = Integer.parseInt(request.getParameter("OVNT_CNT"));
        } catch (NumberFormatException e) {
            throw new RuntimeException(e);
        }

        String inhsCustNm = request.getParameter("INHS_CUST_NM");
        String inhsCustTelNo2 = request.getParameter("INHS_CUST_TEL_NO2");
        String inhsCustTelNo3 = request.getParameter("INHS_CUST_TEL_NO3");
        String inhsCustTelNo4 = request.getParameter("INHS_CUST_TEL_NO4");
        String rsrvCustNm = request.getParameter("RSRV_CUST_NM");
        String rsrvCustTelNo2 = request.getParameter("RSRV_CUST_TEL_NO2");
        String rsrvCustTelNo3 = request.getParameter("RSRV_CUST_TEL_NO3");
        String rsrvCustTelNo4 = request.getParameter("RSRV_CUST_TEL_NO4");

        boolean bindFlag = true;

        if (custNo == null || custNo.equals(""))
            bindFlag = false;
        if (custIdntNo == null || custIdntNo.equals(""))
            bindFlag = false;
        if (rsrvNo == null || rsrvNo.equals(""))
            bindFlag = false;
        if (arrvDate == null || arrvDate.equals(""))
            bindFlag = false;
        if (inhsCustNm == null || inhsCustNm.equals(""))
            bindFlag = false;
        if (inhsCustTelNo2 == null || inhsCustTelNo2.equals(""))
            bindFlag = false;
        if (inhsCustTelNo3 == null || inhsCustTelNo3.equals(""))
            bindFlag = false;
        if (inhsCustTelNo4 == null || inhsCustTelNo4.equals(""))
            bindFlag = false;
        if (rsrvCustNm == null || rsrvCustNm.equals(""))
            bindFlag = false;
        if (rsrvCustTelNo2 == null || rsrvCustTelNo2.equals(""))
            bindFlag = false;
        if (rsrvCustTelNo3 == null || rsrvCustTelNo3.equals(""))
            bindFlag = false;
        if (rsrvCustTelNo4 == null || rsrvCustTelNo4.equals(""))
            bindFlag = false;


        if (!bindFlag)
            throw new RuntimeException("값이 비어있거나 올바르지 않습니다.");

        RsrvModRqstAttr rsrvModrqstAttr = new RsrvModRqstAttr(custNo, custIdntNo, rsrvNo, arrvDate, ovntCnt, inhsCustNm, inhsCustTelNo2, inhsCustTelNo3, inhsCustTelNo4, rsrvCustNm, rsrvCustTelNo2, rsrvCustTelNo3, rsrvCustTelNo4);

        List<RsrvModRqstAttr> temp = new ArrayList<>();
        temp.add(rsrvModrqstAttr);

        dataObj = new RsrvModRqstData(temp);



    }
}
