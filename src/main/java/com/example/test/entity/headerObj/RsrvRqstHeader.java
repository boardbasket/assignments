package com.example.test.entity.headerObj;

import com.example.test.entity.dataObj.RsrvRqstData;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RsrvRqstHeader {
    @SerializedName("Data")
    private RsrvRqstData dataObj;
}
