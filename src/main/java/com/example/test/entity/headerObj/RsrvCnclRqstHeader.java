package com.example.test.entity.headerObj;

import com.example.test.entity.attrObj.RsrvCnclRqstAttr;
import com.example.test.entity.dataObj.RsrvCnclRqstData;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class RsrvCnclRqstHeader {

    public RsrvCnclRqstHeader(HttpServletRequest req){

        String CUST_NO=  req.getParameter("CUST_NO");
        String RSRV_NO = req.getParameter("RSRV_NO");

        if(CUST_NO==null||CUST_NO.equals(""))
            throw new RuntimeException("필드가 비어있습니다.");
        else if (RSRV_NO==null||RSRV_NO.equals(""))
            throw new RuntimeException("필드가 비어있습니다.");

        RsrvCnclRqstAttr rsrvCnclRqstAttr = new RsrvCnclRqstAttr(CUST_NO,RSRV_NO);

        List<RsrvCnclRqstAttr> temp = new ArrayList<>();
        temp.add(rsrvCnclRqstAttr);
        dataObj = new RsrvCnclRqstData(temp);

    }

    @SerializedName("Data")
    private RsrvCnclRqstData dataObj;


}
