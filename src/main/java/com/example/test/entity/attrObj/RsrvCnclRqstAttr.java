package com.example.test.entity.attrObj;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

@AllArgsConstructor
public class RsrvCnclRqstAttr {
    @SerializedName("CUST_NO")
    private String custNo;

    @SerializedName("RSRV_NO")
    private String rsrvNo;

}
