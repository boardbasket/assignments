package com.example.test.servlet;

import com.example.test.entity.headerObj.RsrvModRqstHeader;
import lombok.Getter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/rsrvMod")
@Getter
public class RsrvModServlet extends AbstractServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

        RsrvModRqstHeader rsrvModRqstHeader = new RsrvModRqstHeader(req);// httpServletRequest 객체로 초기화
        String jsonValue  =gson.toJson(rsrvModRqstHeader);

        postUrlConnection("http://localhost:18080/rsrvMod",jsonValue);
    }

}