package com.example.test.servlet;

import lombok.Getter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//예약 요청 받았을때 처리하는 서블릿
@Getter
@WebServlet("/rsrvRqst")
public class RsrvRqstServlet extends AbstractServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
        //RsrvRqstRstHeader result = processFileRead(req,res, RsrvRqstHeader.class, RsrvRqstRstHeader.class);
    }

}