package com.example.test.servlet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.http.HttpServlet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;


public class AbstractServlet extends HttpServlet {

    protected Gson gson = new GsonBuilder().setPrettyPrinting().create();

    protected Map<String, Object> postUrlConnection(String address, String jsonValue) throws IOException {
        BufferedReader br = null;
        try {
            Map<String, Object> result;
            URL url = new URL(address);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            OutputStream os = con.getOutputStream();

            byte[] input = jsonValue.getBytes(StandardCharsets.UTF_8);
            os.write(input);
            con.connect();

            br = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));

            result = gson.fromJson(br, Map.class);

            con.disconnect();

            return result;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if(br!=null)
                br.close();
        }

    }


}