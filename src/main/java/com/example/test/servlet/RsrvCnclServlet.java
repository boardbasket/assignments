package com.example.test.servlet;

import com.example.test.entity.headerObj.RsrvCnclRqstHeader;
import lombok.Getter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

@WebServlet("/rsrvCncl")
@Getter
public class RsrvCnclServlet extends AbstractServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

        RsrvCnclRqstHeader rsrvCnclRqstHeader = new RsrvCnclRqstHeader(req);// httpServletRequest 객체로 초기화
        String jsonValue  =gson.toJson(rsrvCnclRqstHeader);
        Map<String,Object> result = postUrlConnection("http://localhost:18080/rsrvCncl",jsonValue);

        PrintWriter out = res.getWriter();
        String temp = result.toString();
        out.write("<html>");
        out.write("<h1>"+temp+"</h1>");
        out.write("</html>");

    }

}