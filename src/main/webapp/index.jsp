<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<style>

    #inputForm .was-validated:invalid {
        border: 2px solid darkred;
    }

    #inputForm .was-validated:valid {
        border: 1px solid forestgreen;
    }

</style>
<body>
<h1><%= "입력폼(예약취소)" %>
</h1>
<br/>

<div id="container">
    <form action="/rsrvCncl" method="post" id="inputForm">
        <label for="CUST_NO_LABEL">고객번호</label><input id="CUST_NO_LABEL" type="text" value="0000000002" disabled>
        <br>
        <input name="CUST_NO" id="CUST_NO" type="hidden" value="0000000002">
        <label for="RSRV_NO">예약번호</label><input name="RSRV_NO" id="RSRV_NO" type="text" value="" required minlength="10"
                                                maxlength="10" pattern="[0-9]{10}">
        <button type="submit">보내기</button>
    </form>
</div>
</body>
<script>
    document.querySelectorAll("#inputForm input").forEach(elem => {
        elem.addEventListener("invalid", e => {
            e.target.classList.add("was-validated");
        })
    })

</script>


</html>