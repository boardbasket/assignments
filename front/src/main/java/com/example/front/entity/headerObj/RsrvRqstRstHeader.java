package com.example.front.entity.headerObj;

import com.example.front.entity.dataObj.RsrvRqstRstData;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RsrvRqstRstHeader {
    @SerializedName("Data")
    private RsrvRqstRstData dataObj;
}
