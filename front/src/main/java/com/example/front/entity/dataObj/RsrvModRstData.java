package com.example.front.entity.dataObj;

import com.example.front.entity.attrObj.RsrvModRstAttr;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@Getter
@Setter
@ToString
public class RsrvModRstData {

    @SerializedName("ds_prcsResult")
    List<RsrvModRstAttr> list;
}
