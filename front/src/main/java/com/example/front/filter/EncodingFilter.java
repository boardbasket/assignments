package com.example.front.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;

@WebFilter(filterName="encodingFilter",urlPatterns = {"/*"},initParams ={@WebInitParam(name="encoding",value="UTF-8")})
public class EncodingFilter implements Filter {

    private String encoding;
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding(encoding);//get,post로 보내는 값 인코딩 설정
        servletResponse.setCharacterEncoding(encoding);//서버측에서 데이터를 클라이언트로 보낼때 문자를 인코딩 하는 방식
        servletResponse.setContentType("text/html; charset="+encoding);//데이터의 종류 설정
        filterChain.doFilter(servletRequest, servletResponse);//다음 필터체인으로 넘김
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
       encoding = filterConfig.getInitParameter("encoding");
    }
}
