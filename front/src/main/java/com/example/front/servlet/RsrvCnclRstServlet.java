package com.example.front.servlet;

import com.example.front.entity.headerObj.RsrvCnclRstHeader;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/rsrvCncl")
public class RsrvCnclRstServlet extends AbstractServlet{


    private final static String RESOURCE = "{\n" +
            "    \"MessageHeader\": {\n" +
            "        \"MSG_PRCS_RSLT_CD\": \"0\",\n" +
            "        \"MSG_DATA_SUB_RPTT_CNT\": 1,\n" +
            "        \"MSG_ETC\": null,\n" +
            "        \"MSG_DATA_SUB\": [\n" +
            "            {\n" +
            "                \"MSG_INDC_CD\": \"1\",\n" +
            "                \"MSG_CD\": \"SCMI000001\",\n" +
            "                \"MSG_CTNS\": \"정상적으로 처리되었습니다.\"\n" +
            "            }\n" +
            "        ]\n" +
            "    },\n" +
            "    \"Data\": {\n" +
            "        \"ds_prcsResult\": [\n" +
            "            {\n" +
            "                \"PROC_DS\": \"2023-05-23 15:31:37.023\",\n" +
            "                \"PROC_CD\": null,\n" +
            "                \"CUST_NO\": \"0000000002\",\n" +
            "                \"LOC_CD\": \"2101\",\n" +
            "                \"ARRV_DATE\": \"20230716\",\n" +
            "                \"OVNT_CNT\": 2,\n" +
            "                \"CHKOT_EXPT_DATE\": \"20230718\",\n" +
            "                \"RSRV_CUST_NM\": \"조아현\",\n" +
            "                \"RSRV_CUST_TEL_NO2\": \"010\",\n" +
            "                \"RSRV_CUST_TEL_NO3\": \"0000\",\n" +
            "                \"RSRV_CUST_TEL_NO4\": \"0000\",\n" +
            "                \"INHS_CUST_NM\": \"조아현\",\n" +
            "                \"INHS_CUST_TEL_NO2\": \"010\",\n" +
            "                \"INHS_CUST_TEL_NO3\": \"0000\",\n" +
            "                \"INHS_CUST_TEL_NO4\": \"0000\",\n" +
            "                \"RSRV_DATE\": \"20230504\",\n" +
            "                \"CUST_IDNT_NO\": \"195176\"\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "}";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processPost(req,resp, RsrvCnclRstHeader.class,RESOURCE);
    }
}
