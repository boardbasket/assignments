package com.example.front.servlet;

import com.example.front.entity.headerObj.RsrvRqstRstHeader;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/rsrvRqst")
public class RsrvRstServlet extends AbstractServlet{

    private final static String RESOURCE ="{\n" +
            "  \"MessageHeader\": {\n" +
            "    \"MSG_PRCS_RSLT_CD\": \"0\",\n" +
            "    \"MSG_DATA_SUB_RPTT_CNT\": 1,\n" +
            "    \"MSG_ETC\": null,\n" +
            "    \"MSG_DATA_SUB\": [\n" +
            "      {\n" +
            "        \"MSG_INDC_CD\": \"1\",\n" +
            "        \"MSG_CD\": \"SCMI000001\",\n" +
            "        \"MSG_CTNS\": \"정상적으로 처리되었습니다.\"\n" +
            "      }\n" +
            "    ]\n" +
            "  },\n" +
            "  \"Data\": {\n" +
            "    \"ds_prcsResult\": [\n" +
            "      {\n" +
            "        \"PROC_DS\": \"2023-05-23 15:29:32.335\",\n" +
            "        \"PROC_CD\": \"00\",\n" +
            "        \"CUST_NO\": \"0000000002\",\n" +
            "        \"MEMB_NO\": \"161102314\",\n" +
            "        \"RSRV_NO\": \"2308743091\",\n" +
            "        \"ROOM_RATE\": \"131000\"\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "}";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processPost(req,resp, RsrvRqstRstHeader.class,RESOURCE);
    }
}
