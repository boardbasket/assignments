package com.example.front.servlet;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class AbstractServlet extends HttpServlet {

    private Gson gson = new Gson();


    private static final String FILE_SUFFIX = ".txt";
    private static final String FILE_PREFIX = "/";

    protected <T> void processPost(HttpServletRequest request, HttpServletResponse response, Class<T> responseEntityClass, String resourcesName) throws IOException {
        BufferedReader br = null;
        BufferedWriter bw = null;

        try {
            br = new BufferedReader(request.getReader());

            JsonElement jsonElement = gson.fromJson(br, JsonElement.class);

            System.out.println(jsonElement.toString());

            T result = parseGson(resourcesName, responseEntityClass);
            String resultJson = gson.toJson(result);
            bw = new BufferedWriter(response.getWriter());
            bw.write(resultJson);

        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (bw != null)
                bw.close();
            if (br != null)
                br.close();
        }
    }

    protected <T, K> K processFileRead(HttpServletRequest req, HttpServletResponse res, Class<T> requestHeaderCls, Class<K> responseHeaderCls) throws IOException {
        String rqstName = req.getParameter("rqstName");
        String rstName = req.getParameter("rstName");
        String message;
        T rqstJSON;
        K rstJSON;

        try {
            rqstJSON = parseGson(rqstName, requestHeaderCls);
            System.out.println("바인딩 된 객체의 데이터");
            System.out.println(rqstJSON.toString());

            rstJSON = parseGson(rstName, responseHeaderCls);
            message = rstJSON.toString();
            return rstJSON;
        } catch (RuntimeException e) {
            message = e.getCause().getMessage();
            PrintWriter out = res.getWriter();
            out.println("<html><body>");
            out.println("<h1>" + message + "</h1>");
            out.println("</body></html>");
            return null;
        }
    }

    protected <T> T parseGson(String resource, Class<T> param) throws RuntimeException, IOException {

        T rst;
        InputStream is;
        InputStreamReader isr;
        BufferedReader br = null;
        try {
          /*  is = getClass().getResourceAsStream(FILE_PREFIX + resourceName + FILE_SUFFIX);

            if (is == null)
                throw new IOException("파일을 찾을 수 없습니다."); //파일을 resource 에서 못찾을 경우

            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);*/
            rst = gson.fromJson(resource, param);

        } catch (NullPointerException e) {
            throw new IOException("파일 이름이 null 입니다");  //파일이름이 null 일때
        } catch (Exception e) {
            throw new RuntimeException(e); //그 외 오류 Exception 으로  묶어서 처리
        } finally {
            if (br != null)
                br.close();
        }

        return rst;
    }
}
